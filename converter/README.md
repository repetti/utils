Utility to convert group (directory tree) of file. E.g., to encode music collection with some other codec.

Can:
* Rename files (regex)
* Rename directories (also flatten them)

Cannot:
* GUI
* CLI

Paths and everything is hard-coded. C'est la vie. Should work at least in Linux.
