package org.repetti.utils.converter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.stream.Stream;

public class SingleThreadExecutor {

    private Path logPath;

    public Path getLogPath() {
        return logPath;
    }

    public void setLogPath(Path logPath) {
        this.logPath = logPath;
    }


    public static void convertSingle(Path from, Path to, Path logFile) throws IOException, InterruptedException {
        ProcessBuilder pb = new ProcessBuilder(
                "opusenc",
                "--bitrate",
                "128",
                "--vbr",
                from.toAbsolutePath().toString(),
                to.toAbsolutePath().toString());
        pb.redirectErrorStream(true);
        if (logFile != null) {
            pb.redirectOutput(ProcessBuilder.Redirect.appendTo(logFile.toFile()));
        }
        Process p = pb.start();
        int res = p.waitFor();
        if (res != 0) {
            throw new RuntimeException("Exit value = " + res);
        }
    }


    public int convert(Path oldParent, Stream<Path> matchingFiles, NameConverter nc) {
        final int[] counter = {0};
        Path logFile = logPath == null ? null : logPath.resolve("convert+" + System.currentTimeMillis() + ".log");
        matchingFiles.forEach(oldPath -> {
            try {
                System.out.print(oldPath.getFileName().toString());
                convertSingle(oldPath, nc.newPath(oldParent, oldPath, true), logFile);
                System.out.println("... ok");
            } catch (RuntimeException e) {
                System.err.println("Failed to process " + oldPath.relativize(oldParent));
                e.printStackTrace();
                throw e;
            } catch (Exception e) {
                System.err.println("Failed to process " + oldPath.relativize(oldParent));
                e.printStackTrace();
                throw new RuntimeException(e);
            }
            counter[0]++;
        });
        return counter[0];
    }


    public static int dryRun(Path oldParent, Stream<Path> matchingFiles, NameConverter nc) {
        final int[] counter = {0};
        matchingFiles.forEach(oldPath -> {
            try {
                System.out.println(oldPath + " => " + nc.newPath(oldParent, oldPath));
            } catch (RuntimeException e) {
                System.err.println("Failed to process " + oldPath.relativize(oldParent));
                e.printStackTrace();
                throw e;
            } catch (Exception e) {
                System.err.println("Failed to process " + oldPath.relativize(oldParent));
                e.printStackTrace();
                throw new RuntimeException(e);
            }
            counter[0]++;
        });
        return counter[0];
    }
}
