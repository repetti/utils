package org.repetti.utils.converter;

import java.nio.file.Path;
import java.nio.file.Paths;

public class NameConverter {

    private final String[] pathRegex;
    private final String[] fileRegex;
    private final Path newPath;
    private final boolean changePath;
    private final boolean changeFile;

    private NameConverter(String[] pathRegex, String[] fileRegex, Path newPath, boolean changePath, boolean changeFile) {
        this.pathRegex = pathRegex;
        this.fileRegex = fileRegex;
        this.newPath = newPath;
        this.changePath = changePath;
        this.changeFile = changeFile;
    }

    public Path newPath(Path oldParent, Path old) {
        return this.newPath(oldParent, old, false);
    }

    public Path newPath(Path oldParent, Path old, boolean create) {
        Path relPath = oldParent.relativize(old.getParent());
        final Path newFilePath;
        newFilePath = changePath ?
                newPath.resolve(relPath.toString().replaceAll(pathRegex[0], pathRegex[1])) :
                newPath.resolve(relPath); //.toString()
        if (create && !newFilePath.toFile().exists()) {
            boolean res = newFilePath.toFile().mkdirs();
            if (!res) {
                throw new RuntimeException("Unable to create directory: " + newFilePath);
            }
        }
        return Paths.get(
                newFilePath.toAbsolutePath().toString(),
                changeFile ?
                        old.getFileName().toString().replaceAll(fileRegex[0], fileRegex[1]) :
                        old.getFileName().toString());
    }

    public static class Builder {
        private String[] pathRegex;
        private String[] fileRegex;
        private Path newPath;
        private boolean changePath = true;
        private boolean changeFile = true;

        public Builder path(Path newPath) {
            if (!newPath.toFile().exists()) {
                throw new IllegalArgumentException("Path does not exist");
            } else if (!newPath.toFile().isDirectory()) {
                throw new IllegalArgumentException("Path is not a directory");
            }
            this.newPath = newPath;
            return this;
        }

        public Builder path(String newPath) {
            return this.path(Paths.get(newPath));
        }

        public Builder pathRegexNone() {
            changePath = false;
            return this;
        }

        /**
         * Regexp to rename the paths
         * @param from something like "/"
         * @param to could be "-"
         * @return this builder
         */
        public Builder pathRegex(String from, String to) {
            changePath = true;
            pathRegex = new String[]{from, to};
            return this;
        }

        /**
         * Do not rename the file. Can normally happen if re-encoding (e.g. with lower bitrate).
         */
        public Builder fileRegexNone() {
            changeFile = false;
            return this;
        }

        /**
         * Regexp to rename the files
         * @param from something like ".flac$"
         * @param to could be ".opus"
         * @return this builder
         */
        public Builder fileRegex(String from, String to) {
            changeFile = true;
            fileRegex = new String[]{from, to};
            return this;
        }

        /**
         * Should be run after specifying path, pathRegex and fileRegex
         * @return New ready-to-use NameConverter object
         * @throws IllegalStateException if not properly initialized
         */
        public NameConverter build() {
            if (newPath != null && (!changePath || pathRegex != null) && (!changeFile || fileRegex != null)) {
                return new NameConverter(pathRegex, fileRegex, newPath, changePath, changeFile);
            }
            throw new IllegalStateException("Something was not specified");
        }
    }
}
