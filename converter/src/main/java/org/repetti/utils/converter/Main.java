package org.repetti.utils.converter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) throws IOException {
        Path source = Paths.get("/media/path-to-files/flac");
        Path target = Paths.get("/dev/shm");

        simple(source, target, "Faye.*\\.flac");
    }


    private static void simple(String from, String to, String fileRegex) throws IOException {
        Path source = Paths.get(from);
        Path target = Paths.get(to);
        simple(source, target, fileRegex);
    }

    private static void simple(Path source, Path target, String fileRegex) throws IOException {

        NameConverter nc = new NameConverter.Builder()
                .fileRegex("flac$","opus")
//                .pathRegex("/","-")
                .pathRegexNone()
                .path(target)
                .build();
//        matchingFiles(source, fileRegex).forEach(System.out::println);
        SingleThreadExecutor x = new SingleThreadExecutor();
        x.setLogPath(target);
        int res = x.convert(source, matchingFiles(source, fileRegex), nc);
//        int res = SingleThreadExecutor.dryRun(source, matchingFiles(source, fileRegex), nc);
        System.out.println("Successfully converted " + res + " files");
    }


    private static Stream<Path> matchingFiles(Path origin, String regex) throws IOException {
        Pattern pattern = Pattern.compile(regex);
        Predicate<String> stringPredicate = pattern.asPredicate();
        Predicate<Path> predicate = (p) -> stringPredicate.test(p.toString());
        return Files.walk(origin).filter(predicate);
    }

}
